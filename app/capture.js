'use strict';
const MK = require('react-native-material-kit');
const appStyles = require('./styles');

import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
} from 'react-native';
const {
  MKButton,
  MKColor,
} = MK;
import Camera from 'react-native-camera';

const styles = Object.assign({}, appStyles, StyleSheet.create({
  container: {
   flex: 1
 },
 preview: {
   flex: 1,
   justifyContent: 'flex-end',
   alignItems: 'center',
   height: Dimensions.get('window').height,
   width: Dimensions.get('window').width
 },
 fab:{
   marginBottom: 50,
 }
}));

const CaptureButton = MKButton.accentColoredFab()
  .withStyle(styles.fab)
  .build();

class Cams extends Component{
  render(){
    return(
      <View style={styles.container}>
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}>
          <CaptureButton onPress={this.takePicture.bind(this)}>
            <Image pointerEvents="none" source={require('../img/plus_white.png')} />
          </CaptureButton>
        </Camera>
      </View>
    )
  }

  takePicture() {
    this.camera.capture()
      .then((data) => console.log(data))
      .catch(err => console.error(err));
  }
}

module.exports = Cams;
