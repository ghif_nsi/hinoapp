'use strict';
import React, { Component } from 'react';
const appStyles = require('./styles');

import {
  AppRegistry,
  StyleSheet,
  View,
  Text,
  ScrollView,
  ListView,
  TextInput,
  TouchableOpacity,
  ToolbarAndroid,
  BackAndroid,
} from 'react-native';

import {
  MKTextField,
  MKColor,
  MKButton,
  MKRadioButton
} from 'react-native-material-kit';

var Capture = require('./capture');

const styles = Object.assign({}, appStyles, StyleSheet.create({
  TextField: {
    height: 28,  // have to do it on iOS
  },
  saveButton :{
  },
  multiLine:{
    borderBottomColor: '#000000',
    borderBottomWidth: 2
  }

}));

const Textfield = MKTextField.textfield()
  .withPlaceholder('...')
  .withStyle(styles.TextField)
  .build();

const SaveButton = MKButton.coloredFlatButton()
    .withText('SAVE')
    .withStyle(styles.saveButton)
    .build();

const AddPicButton = MKButton.coloredFlatButton()
    .withText('Add Picture')
    .withStyle(styles.saveButton)
    .build();

class Form extends Component{

  constructor(){
    super();
    this.radioGroup = new MKRadioButton.Group();
  }

  render(){
      return(
        <ScrollView style={styles.scrollView} contentContainerStyle={styles.container}>
          <ScrollView>
            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Main Dealer</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Technical Problem</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tipe Unit</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kode Dealer</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kecepatan (Max.)</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Muatan (Max.)</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>No. Rangka</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>No. Mesin</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>KM/Jam</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kg</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Nomer Laporan</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tanggal Penyerahan Unit Baru</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tanggal Kerusakan</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Mileage</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tipe Body</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Jenis Muatan</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Main Dealer</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Technical Problem</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tipe Unit</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kode Dealer</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kecepatan (Max.)</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Muatan (Max.)</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>No. Rangka</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>No. Mesin</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>KM/Jam</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kg</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Nomer Laporan</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tanggal Penyerahan Unit Baru</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tanggal Kerusakan</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Mileage</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tipe Body</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Masa Waktu Kerusakan</Text>
              <View style={styles.row}>
                <View style={styles.col}>
                  <MKRadioButton checked={true} group={this.radioGroup}/>
                  <Text style={styles.legendLabel}>Masa Garansi</Text>
                </View>
                <View style={styles.col}>
                  <MKRadioButton group={this.radioGroup}/>
                  <Text style={styles.legendLabel}>Diluar Masa Garansi</Text>
                </View>
                <View style={styles.col}>
                  <MKRadioButton group={this.radioGroup}/>
                  <Text style={styles.legendLabel}>PDI</Text>
                </View>
              </View>
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kronologis Kejadian</Text>
              <TextInput
                style={styles.multiLine}
                multiline = {true}
                numberOfLines = {4}/>
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Data Operasional</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Area Operasional</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kondisi Jalan</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Jarak Tempuh</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Trip per Hari</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kondisi Sebelum Dibongkar</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kondisi Seteleah Dibongkar</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Estimasi Kerusakan Part Body</Text>
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kesimpulan Masalah / Problem</Text>
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Analisa Masalah</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Penyebab Kerusakan</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Tindakan Perbaikan</Text>
              <View style={styles.row}>
                <View style={styles.col}>
                  <MKRadioButton checked={true} group={this.radioGroup}/>
                  <Text style={styles.legendLabel}>Ganti Baru</Text>
                </View>
                <View style={styles.col}>
                  <MKRadioButton group={this.radioGroup}/>
                  <Text style={styles.legendLabel}>Pemasangan / Penyetelan Ulang</Text>
                </View>
                <View style={styles.col}>
                  <Text style={styles.legendLabel}>Lain - Lain</Text>
                  <Textfield />
                </View>
              </View>
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Komentar/Permintaan Customer</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Kerusakan Disebabkan Kesalahan Dari</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Nama Pemilik</Text>
              <Textfield />
            </View>

            <View stye={styles.row}>
              <Text style={styles.legendLabel}>Alamat Pemilik</Text>
              <Textfield />
            </View>

            <AddPicButton onPress={() => {this.props.navigator.push({title: 'Add Picture',component: Capture,});}}/>

          </ScrollView>
          <SaveButton/>
        </ScrollView>

      );
  }
}

module.exports = Form;
