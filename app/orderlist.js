'use strict';
import React, { Component } from 'react';
const appStyles = require('./styles');

import {
  StyleSheet,
  View,
  Image,
  ScrollView,
  ListView,
  Text,
} from 'react-native';

import {
  MKTextField,
  MKColor,
  MKButton
} from 'react-native-material-kit';

const styles = Object.assign({}, appStyles, StyleSheet.create({
    addButton :{
      position : 'absolute',
      bottom: 10,
      right: 10,
    },
    imgSmall :{
      width: 50,
      height: 50
    },
    orderListView:{
      flex:1,
      flexDirection: 'row'
    }

  }));

var NewForm = require('./form');

const AddButton = MKButton.accentColoredFab()
  .withStyle(styles.addButton)
  .build();

class OrderList extends Component{

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      dataSource : ds.cloneWithRows([
        {
          orderNo: "1",
          technicalProblem: "BUSING KOCAK 1",
          image: "http://hino.co.id/media/images/original/product/20130524014943-list-product01.png"
        },{
          orderNo: "2",
          technicalProblem: "BUSING NGACO 2",
          image: "http://hino.co.id/media/images/original/product/20150114043253-newhino500rangerthumb.png"
        }
      ])
    };
  }

  render(){
    return(
      <ScrollView style={styles.scrollView} contentContainerStyle={styles.container}>
          <ListView renderRow={this.renderList} dataSource={this.state.dataSource}/>
          <AddButton onPress={() => {this.props.navigator.push({title: 'New Order',component: NewForm,});}}>
            <Image pointerEvents="none" source={require('../img/plus_white.png')} />
          </AddButton>
      </ScrollView>
    );
  }

  renderList = (orderListData) =>{
    console.log(orderListData.image);
    return(
      <View style={styles.orderListView}>
        <Image style={styles.imgSmall} source={{uri : orderListData.image}} />
          <View>
            <Text style={styles.pushLabel}>{orderListData.orderNo}</Text>
            <Text style={styles.pushLabel}>{orderListData.technicalProblem}</Text>
          </View>
      </View>
    );
  };

}

module.exports = OrderList;
