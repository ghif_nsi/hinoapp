# README #

Hino mobile apps

### What is this repository for? ###

* Android Apps
* Using react-native

### How do I get set up? ###
Prerequisite:

* Android Studio
* Nodejs (for NPM)
* Phyton 2
* (Optional) Yarn, for faster react-native installation.

How To Run:

* install react native using npm

```
#!

npm install -g react-native
```

* install react native material kit 

```
#!

npm install -S react-native-material-kit
react-native link react-native-material-kit
```

* run command 
```
#!

react-native run-android
react-native start
```


### Contribution guidelines ###

* Just Push :)