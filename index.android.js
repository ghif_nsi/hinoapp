'use strict';

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  View,
  Text,
  ScrollView,
  Navigator,
  TouchableOpacity,
  ToolbarAndroid,
  BackAndroid,
} from 'react-native';

import { setTheme, MKColor } from 'react-native-material-kit';

setTheme({
  primaryColor: MKColor.Teal,
  primaryColorRGB: MKColor.RGBTeal,
  accentColor: MKColor.Amber,
});

var OrderList = require('./app/orderlist');
var NewForm = require('./app/form');

function renderScreen(route, navigator) {
  return (
    <View style={styles.container}>
    <route.component
    {...route.passProps}
    navigator={navigator}
    />
    <ToolbarAndroid
    style={styles.toolbar}
    title={route.title}
    navIcon={require('./img/ic_back.png')}
    onIconClicked={() => navigator.pop()}
    />
    </View>
  );
}

function renderHome(navigator) {
  return (
    <View style={styles.container}>
    <Home navigator={navigator} />
    <ToolbarAndroid
    style={styles.toolbar}
    title="Hino Dealership Companion App"
    />
    </View>
  );
}

class Home extends Component {
  render(){
    return (
      <ScrollView style={styles.list}
      contentContainerStyle={styles.container}>
        <TouchableOpacity onPress={() => {this.props.navigator.push({title: 'Order List',component: OrderList,});}}>
          <Text style={styles.pushLabel}>Order List</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

class hinoapp extends Component {
  render() {
    return (
      <Navigator
      initialRoute={{name: 'home'}}
      renderScene={this.routes}
      />
    );
  }

  routes(route, navigator){
    // this.navigator = navigator;
    //console.log('routing to:', route);
    switch (route.name) {
      case 'home':
      return renderHome(navigator);
      default:
      return renderScreen(route, navigator);
    }
  }

  hardwareBackPress(){
    if (!this.navigator) {
      return false;
    }

    var currentRoutes = this.navigator.getCurrentRoutes();
    if (currentRoutes[currentRoutes.length - 1].name !== 'home') {
      // if not on main screen
      // go back to main screen
      this.navigator.popToTop();
      return true;
    }
    // else minimize the application
    return false;
  }

  componentWillMount() {
    BackAndroid.addEventListener('hardwareBackPress', this.hardwareBackPress);
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this.hardwareBackPress);
  }
}

var styles = StyleSheet.create({
  toolbar: {
    backgroundColor: 'rgba(245,252,255,.98)',
    height: 56,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  list: {
    backgroundColor: '#F5FCFF',
    paddingTop: 64,
  },
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginTop: 20, marginBottom: 0,
  },
  pushLabel: {
    padding: 10,
    color: '#2196F3',
    textAlign: 'center',
  },
});

AppRegistry.registerComponent('hinoapp', () => hinoapp);
